import 'package:midterm_1/midterm_1.dart' as midterm_1;
import 'dart:io';

List tokenizing(String tokeniz) {
  List res = [];
  for (var i = 0; i < tokeniz.length; i++) {
    if (tokeniz[i] == ' ') {
      res.add(tokeniz[i]);
      res.remove(tokeniz[i]);
    } else {
      res.add(tokeniz[i]);
    }
  }
  res = tokenizingNeg(res);
  return res;
}

List tokenizingNeg(List token) {
  for (var i = 0; i < token.length - 1; i++) {
    for (var j = i + 1; j < token.length; j++) {
      if (token[i] == '-' && token[j] == '(') {
        break;
      } else if (token[i] == '-') {
        var temp = token[j];
        token[i] = '-$temp';
        token.remove(token[j]);
      }
    }
  }
  return token;
}

bool isNumeric(String s) {
  if (s == null) {
    return false;
  }
  return double.tryParse(s) != null;
}

int Precedence(String operator) {
  if (operator == "(" || operator == ")") {
    return 4;
  }
  if (operator == "^") {
    return 3;
  }
  if (operator == "*" || operator == "/") {
    return 2;
  }
  if (operator == "+" || operator == "-") {
    return 1;
  }

  return 0;
}

List toPostfixExpression(List infix) {
  List operators = [];
  List postfix = [];
  infix.forEach((i) {
    if (isNumeric(i)) {
      postfix.add(i);
    } else if (!isNumeric(i) && i != '(' && i != ')') {
      while (operators.isNotEmpty &&
          operators.last != '(' &&
          Precedence(i) < Precedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(i);
    } else if (i == '(') {
      operators.add(i);
    } else if (i == ')') {
      while (operators.last != '(') {
        postfix.add(operators.removeLast());
      }
      operators.remove('(');
    }
  });
  while (operators.isNotEmpty) {
    postfix.add(operators.removeLast());
  }
  return postfix;
}

double evaluatepostfix(List postfix) {
  List val = [];
  double left, right;
  postfix.forEach((i) {
    if (isNumeric(i)) {
      double temp = double.parse(i);
      val.add(temp);
    } else {
      right = val.removeLast();
      left = val.removeLast();
      var res = cal(left, right, i);
      val.add(res);
    }
  });
  return val.first;
}

double cal(double left, double right, String operator) {
  double res = 0;
  switch (operator) {
    case "+":
      {
        res = left + right;
      }
      break;
    case "-":
      {
        res = left - right;
      }
      break;
    case "*":
      {
        res = left * right;
      }
      break;
    case "/":
      {
        res = left / right;
      }
      break;
    case "%":
      {
        res = left % right;
      }
      break;
    case "^":
      {
        res = exp(left, right);
      }
      break;
    default:
      {}
      break;
  }
  return res;
}

double exp(double left, double right) {
  double exponent = left;
  double res = 1;
  for (int i = 0; i < right; i++) {
    res = res * exponent;
  }

  return res;
}

void main(List<String> arguments) {
  print('Input infix expression: ');
  var exprs = stdin.readLineSync()!;
  var infix = tokenizing(exprs);
  print("your infix : $infix");
  var postfix = toPostfixExpression(infix);
  print("your postfix : $postfix");
}
